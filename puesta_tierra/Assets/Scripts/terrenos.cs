using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class terrenos : MonoBehaviour
{

    [SerializeField] private GameObject terrenoOriginal;
    [SerializeField] private GameObject terrenoHueco;
    [SerializeField] private GameObject terrenoRelleno;
    [SerializeField] private GameObject quitarCesped;

    [SerializeField] private GameObject rayoPararrayo;

    [SerializeField] private ParticleSystem polvoPS;
    [SerializeField] private ParticleSystem ondaElectrica;

    private Collider otherCollider;
    private int numeroDeFuncion = 0;
    private bool dentroDelTrigger = false;

    [SerializeField] private detectarTierra detectarTierra;

    private bool tierra_lista;


    void Start()
    {
    }

    void Update()
    {
        // Verificar si se ha hecho clic y si estamos dentro del trigger
        if (dentroDelTrigger && Input.GetMouseButtonDown(0))
        {
            numeroDeFuncion++;
            EjecutarFuncion();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // Al entrar en el trigger, establecer la referencia al objeto y activar el flag
        otherCollider = other;
        dentroDelTrigger = true;
        Debug.Log("Objeto entr� en el terreno.");
        tierra_lista = detectarTierra.tierra_lista;
    }

    private void OnTriggerExit(Collider other)
    {
        // Al salir del trigger, desactivar el flag
        if (other == otherCollider)
        {
            dentroDelTrigger = false;
            otherCollider = null;
            Debug.Log("Objeto sali� del terreno.");
        }
    }

    private void EjecutarFuncion()
    {
        // Alternar entre las funciones seg�n el n�mero de funci�n
        if (dentroDelTrigger)
        {
            switch (numeroDeFuncion)
            {
                case 1:
                    polvoPS.Play();
                    Invoke("ActivarTerrenoHueco", 1.5f);
                    break;
                case 2:
                    break;
                    
                default:
                    // Resetear al iniciar nuevamente
                    numeroDeFuncion = 3;
                    if (tierra_lista)
                    {
                        polvoPS.Play();
                        Invoke("ActivarTerrenoRelleno", 1.5f);
                    }
                    else
                    {
                        Debug.Log("No se puede activar el terreno relleno porque no se ha hecho la puesta a tierra.");
                    }
                    
                    //DesactivarTodos();
                    break;
            }
        }
    }

    private void ActivarTerrenoHueco()
    {
        terrenoOriginal.SetActive(false);
        terrenoHueco.SetActive(true);
        terrenoRelleno.SetActive(false);    
        quitarCesped.SetActive(false);
        
    }

    private void ActivarTerrenoRelleno()
    {
        terrenoOriginal.SetActive(false);
        terrenoHueco.SetActive(false);
        terrenoRelleno.SetActive(true);
        ondaElectrica.Play();
        rayoPararrayo.SetActive(true);
    }

    private void DesactivarTodos()
    {        
        Debug.Log("Reiniciar: Desactivar todos los GameObjects");
    }
}
