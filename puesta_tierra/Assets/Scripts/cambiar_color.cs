using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cambiar_color : MonoBehaviour
{

    // Referencia al material que quieres cambiar
    [SerializeField] private Material material;

    // Color al que quieres cambiar el material
    [SerializeField] private Color apagado;
    [SerializeField] private Color encendido;

    void Start()
    {
        // Verificar si el material est� asignado
        if (material == null)
        {
            Debug.LogError("No se ha asignado ning�n material.");
            return;
        }

        // Cambiar el color del material
        material.color = encendido;
        Debug.Log("Color del material cambiado a: " + encendido);
    }

    void Update()
    {
        // Ejemplo: cambiar el color al presionar la tecla C
        if (Input.GetKeyDown(KeyCode.C))
        {
            CambiarColor();
        }
    }

    public void CambiarColor()
    {
        // Cambiar el color del material
        material.color = apagado;
        Debug.Log("Color del material cambiado a: " + apagado);
    }
}
