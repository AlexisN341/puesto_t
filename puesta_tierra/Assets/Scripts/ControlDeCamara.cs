using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlDeCamara : MonoBehaviour
{

    [SerializeField] private float moveSpeed = 10f;

    [SerializeField] private float rotationSpeed = 100f;

    [SerializeField] private float scrollSpeed = 10f;
    [SerializeField] private float minZoom = 5f;
    [SerializeField] private float maxZoom = 60f;

    [SerializeField] private float flySpeed = 10f; // Velocidad de vuelo

    //private float _currentZoom = 0f;

    private Vector3 _initialPosition;
    private Quaternion _initialRotation;


    private Camera cam;

    private bool canRotate = true;
    private CharacterController characterController;

    // Start is called before the first frame update
    void Start()
    {
        cam = GetComponent<Camera>();
        _initialPosition = transform.position;
        _initialRotation = transform.rotation;

        characterController = GetComponent<CharacterController>();

        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;

    }

    // Update is called once per frame
    void Update()
    {
        HandleMovement();
        HandleZoom();
        HandleRotationToggle();
        if (canRotate)
        {
            HandleRotation();
        }
        HandleFlight();
    }

    private void HandleMovement()
    {
        float moveX = Input.GetAxis("Horizontal");
        float moveZ = Input.GetAxis("Vertical");

        Vector3 move = transform.right * moveX + transform.forward * moveZ;
        characterController.Move(move * moveSpeed * Time.deltaTime);
        //transform.position += move * moveSpeed * Time.deltaTime;
    }

    // Zoom with Scroll
    private void HandleZoom()
    {
        // Obtener el valor del scroll del ratón
        float scroll = Input.GetAxis("Mouse ScrollWheel");

        // Ajustar el campo de visión (FOV) de la cámara basado en el scroll
        if (cam != null)
        {
            cam.fieldOfView -= scroll * scrollSpeed;
            // Limitar el FOV entre minZoom y maxZoom
            cam.fieldOfView = Mathf.Clamp(cam.fieldOfView, minZoom, maxZoom);
        }
    }

    // Rotation with Mouse

    private void HandleRotation()
    {
        float mouseX = Input.GetAxis("Mouse X") * rotationSpeed * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * rotationSpeed * Time.deltaTime;

        Vector3 rotation = transform.eulerAngles;
        rotation.x -= mouseY;
        rotation.y += mouseX;

        transform.eulerAngles = rotation;
    }

    private void HandleRotationToggle()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            canRotate = !canRotate;
        }
    }   

    private void HandleFlight()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            characterController.Move(Vector3.up * flySpeed * Time.deltaTime);
            //transform.position += Vector3.up * flySpeed * Time.deltaTime;
        }
    }
}


