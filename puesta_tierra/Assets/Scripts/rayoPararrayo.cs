using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rayoPararrayo : MonoBehaviour
{

    public Transform final;


    public int cantidad_de_puntos;
    public float dispersion;
    public float frecuencia;

    private LineRenderer lineRenderer;

    private float tiempo = 0;


    // Start is called before the first frame update
    void Start()
    {

        lineRenderer = GetLine();
        
    }

    // Update is called once per frame
    void Update()
    {
        tiempo += Time.deltaTime;

        if (tiempo > frecuencia)
        {
            tiempo = 0;
            ActualizarPuntos(this.lineRenderer);
        }
    }

    private LineRenderer GetLine()
    {
        return GetComponent<LineRenderer>();
    }

    private void ActualizarPuntos(LineRenderer linea)
    {
        List<Vector3> puntos = InterpolarPuntos(Vector3.zero, final.localPosition, cantidad_de_puntos);
        //Debug.Log(final.localPosition);
        linea.positionCount = puntos.Count;
        linea.SetPositions(puntos.ToArray());
    }

    private List<Vector3> InterpolarPuntos(Vector3 inicio, Vector3 fin, int cantidad)
    {
        List<Vector3> puntos = new List<Vector3>();
        
        for(int i = 0; i < cantidad; i++)
        {
            puntos.Add(Vector3.Lerp(inicio, fin, (float) i / cantidad) + RandomDesface());
        }
        return puntos;
    }


    private Vector3 RandomDesface()
    {
        return Random.insideUnitSphere.normalized *Random.Range(0, dispersion);
    }
}
