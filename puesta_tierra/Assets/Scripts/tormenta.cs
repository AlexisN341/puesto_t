using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tormenta : MonoBehaviour
{
    public ParticleSystem lluvia_fx;
    [SerializeField] private ParticleSystem tormenta_fx;

    private bool press_key = false;
    [SerializeField] private detectarTierra detectarTierra;

    //[SerializeField] private terrenos terreno;
    private bool falla_electrica;

    // Referencia al material que quieres cambiar
    [SerializeField] private Material material;

    // Color al que quieres cambiar el material
    //[SerializeField] private Color apagado = 22211F;
    //[SerializeField] private Color encendido;


    // Start is called before the first frame update
    void Start()
    {
        // Cambiar el color del material
        falla_electrica = false;
        onOffEmision(true);
        //material.color = encendido;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Q))
        {
            press_key = !press_key;
            if(press_key)
            {
                iniciarTormenta();
            }
            else
            {
                detenerTormenta();
            }
        }
    }

    private void iniciarTormenta()
    {
        lluvia_fx.Play();
        tormenta_fx.Play();
        falla_electrica = detectarTierra.falla_electrica;
        if (falla_electrica==true)
        {
            //CambiarColor(apagado);
            onOffEmision(false);

        }
        else
        {
            onOffEmision(true);
            //CambiarColor(encendido);
        }
    }

    private void detenerTormenta()
    {

        lluvia_fx.Stop();
        tormenta_fx.Stop();

    }

    void CambiarColor(Color color)
    {
        // Cambiar el color del material
        material.color = color;
        //Debug.Log("Color del material cambiado a: " + apagado);
    }

    void onOffEmision(bool estadoEmision)
    {

        if (estadoEmision)
        {
            // Activa la emisi�n estableciendo el color de emisi�n
            material.EnableKeyword("_EMISSION");
        }
        else
        {
            // Desactiva la emisi�n estableciendo el color de emisi�n a negro
            material.DisableKeyword("_EMISSION");
        }
    }
}
