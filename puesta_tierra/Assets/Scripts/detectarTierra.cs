using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class detectarTierra : MonoBehaviour
{

    private Collider otherCollider;
    private string tagCollider;

    public bool tierra_lista;

    public bool falla_electrica;
    [SerializeField] private Material material;

    [SerializeField] private Transform ubicacion_tierra;

    //[SerializeField] private tormenta tormenta;
    // Start is called before the first frame update

    void Start()
    {
        falla_electrica = true;
        tierra_lista = false;
    }

    private void Update()
    {
        // Verificar si se ha soltado el clic izquierdo
        if (Input.GetMouseButtonUp(0))
        {
            if(otherCollider != null)
            {
                Rigidbody rb = otherCollider.GetComponent<Rigidbody>();
                if (rb != null)
                {
                    Destroy(rb);
                    Debug.Log("Rigidbody eliminado del objeto " + otherCollider.name);
                }


                if (tagCollider == "CableTierra")
                {
                    otherCollider.transform.position = new Vector3(ubicacion_tierra.position.x, ubicacion_tierra.position.y, ubicacion_tierra.position.z);
                    falla_electrica = false;
                    tierra_lista = true;
                    material.EnableKeyword("_EMISSION");
                }
                else if (tagCollider == "Varilla")
                {
                    otherCollider.transform.position = new Vector3(ubicacion_tierra.position.x, 68.515f, ubicacion_tierra.position.z);
                    otherCollider.transform.eulerAngles = Vector3.zero;              


                }
            }

            
        }
    }
    
    private void OnTriggerEnter(Collider other)
    {
        // Verificar si el objeto que entra tiene el tag "CableTierra" y si se ha soltado el clic izquierdo
        if (other.CompareTag("CableTierra"))
        {
            otherCollider = other;
            tagCollider = other.tag;
            Debug.Log("El objeto CableTierra entr� en el collider ");
        }

        if (other.CompareTag("Varilla"))
        {
            otherCollider = other;
            tagCollider = other.tag;
            Debug.Log("El objeto Varilla entr� en el collider ");
        }
    }

}
