using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rayo : MonoBehaviour
{
    [SerializeField] private Transform punto_final;

    [SerializeField] private int cantidad_puntos;

    [SerializeField] private float dispersion;
    [SerializeField] private float frecuencia;

    private LineRenderer linea;
    private float tiempo = 0;
    // Start is called before the first frame update
    void Start()
    {
        linea = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        tiempo += Time.deltaTime;

        if(tiempo >= frecuencia)
        {
            tiempo = 0;
            ActualizarPuntos(this.linea);
        }

    }

    private void ActualizarPuntos(LineRenderer line)
    {
        List<Vector3> puntos = InterpolarPuntos(transform.position, punto_final.position,cantidad_puntos);
        linea.positionCount = puntos.Count;
        linea.SetPositions(puntos.ToArray());
    }

    private List<Vector3> InterpolarPuntos(Vector3 inicio, Vector3 fin, int total_puntos)
    {
        List<Vector3> lista_puntos = new List<Vector3>();

        for (int i = 0; i < total_puntos; i++)
        {
            lista_puntos.Add(Vector3.Lerp(inicio, fin, (float)i / total_puntos) + RandomPuntos());
        }
        return lista_puntos;
    }


    private Vector3 RandomPuntos()
    {
        return Random.insideUnitSphere.normalized * Random.Range(0, dispersion);
    }
}
